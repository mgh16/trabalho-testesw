package cafe;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MaquinaCafeTest {

	// new, pedircafe
	// new, adicionarmoeda(1), pedircafe
	// new, adicionarmoeda(1), pedircafe
	// new, adicionarmoeda(1), pedircafe, pedircafe
	
	@Test
	void TestSemMoedas() {
		MaquinaCafe maquina = new MaquinaCafe();
		boolean serviu = maquina.pedirCafe();
		assertFalse(serviu);
	}
	
	@Test
	void Test1real() throws Exception {
		MaquinaCafe maquina = new MaquinaCafe();
		maquina.adicionarMoeda(1);
		boolean serviu = maquina.pedirCafe();
		assertFalse(serviu);
	}
	
	@Test
	void Test2real() throws Exception {
		MaquinaCafe maquina = new MaquinaCafe();
		maquina.adicionarMoeda(1);
		maquina.adicionarMoeda(1);
		boolean serviu = maquina.pedirCafe();
		assertTrue(serviu);
	}
	
	@Test
	void TestCafeLiberado() throws Exception {
		MaquinaCafe maquina = new MaquinaCafe();
		maquina.adicionarMoeda(1);
		maquina.adicionarMoeda(1);
		maquina.pedirCafe();
		boolean serviu = maquina.pedirCafe();
		assertFalse(serviu);	
	}
	
	//new, adicionarmoeda(!= 1)
	//new, adicionarmoeda(1),  adicionarmoeda(!= 1)
	//new, adicionarmoeda(1), adicionarMoeda(1),  adicionarMoeda(1),
	//new, adicionarmoeda(1), adicionarMoeda(1),  adicionarMoeda(!=1),
	
	@Test
	void TestSemMoedasInsereDiff1() {
		MaquinaCafe maquina = new MaquinaCafe();
		Exception exception = assertThrows(Exception.class, () -> {
			maquina.adicionarMoeda(2);
	    });
	    String expectedMessage = "maquina so aceita um real.";
	    String actualMessage = exception.getMessage();
	    assertTrue(actualMessage.contains(expectedMessage));
	}
	
	@Test
	void Test1RealInsereDiff1() throws Exception {
		MaquinaCafe maquina = new MaquinaCafe();
		maquina.adicionarMoeda(1);
		Exception exception = assertThrows(Exception.class, () -> {
			maquina.adicionarMoeda(2);
	    });
	    String expectedMessage = "maquina so aceita um real.";
	    String actualMessage = exception.getMessage();
	    assertTrue(actualMessage.contains(expectedMessage));
	}
	
	@Test
	void Test2RealInsereDiff1() throws Exception {
		MaquinaCafe maquina = new MaquinaCafe();
		maquina.adicionarMoeda(1);
		maquina.adicionarMoeda(1);
		Exception exception = assertThrows(Exception.class, () -> {
			maquina.adicionarMoeda(2);
	    });
	    String expectedMessage = "maquina so aceita um real.";
	    String actualMessage = exception.getMessage();
	    assertTrue(actualMessage.contains(expectedMessage));
	}
	
	@Test
	void Test2RealInsere1() throws Exception {
		MaquinaCafe maquina = new MaquinaCafe();
		maquina.adicionarMoeda(1);
		maquina.adicionarMoeda(1);
		Exception exception = assertThrows(Exception.class, () -> {
			maquina.adicionarMoeda(1);
	    });
	    String expectedMessage = "maquina nao aceita mais moedas.";
	    String actualMessage = exception.getMessage();
	    assertTrue(actualMessage.contains(expectedMessage));
	}

}






